create
    extension if not exists "uuid-ossp";

create table stocks
(
    id        uuid primary key,
    name      varchar(255),
    timestamp bigint
);

create table prices
(
    id       uuid primary key,
    stock_id uuid         not null,
    name     varchar(255) not null,
    value    float        not null,
    foreign key (stock_id) references stocks (id)
);

create table news
(
    id          uuid primary key,
    title       varchar(255),
    description varchar(500),
    timestamp   bigint,
    author      varchar(255),
    image       varchar(500),
    url         varchar(500),
    keywords    varchar(500)

);

create table tickers
(
    ticker varchar(255) primary key,
    name   varchar(255)

);

COPY  tickers (ticker, name)
    FROM 'C:\Users\ekate\IdeaProjects\Reactive-StockPrices\src\main\resources\db\symbols.csv'
    DELIMITER ','
    CSV HEADER;
