package com.reactive.stock.finance.app.apis;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.function.Function;

public interface FinancePricesHandler extends Function<String, Optional<BigDecimal>> {

}
