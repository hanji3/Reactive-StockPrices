package com.reactive.stock.finance.app.messages;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TickerData {
    @JsonProperty("ticker")
    private String ticker;
    @JsonProperty("name")
    private  String plainName;

    public TickerData() {
    }

    public TickerData(String ticker, String plainName) {
        this.ticker = ticker;
        this.plainName = plainName;
    }

    public String getTicker() {
        return ticker;
    }

    public String getPlainName() {
        return plainName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TickerData that = (TickerData) o;
        return plainName.equals(that.plainName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(plainName);
    }
}
