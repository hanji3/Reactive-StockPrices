package com.reactive.stock.finance.app.apis;

import com.reactive.stock.finance.app.messages.TickerData;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

@Service
public class SymbolIntegration implements TickersHandler {
    private static final String COMMA_DELIMITER = ",";

    @Value("classpath:db/symbols.csv")
    private Resource resource;

    @SneakyThrows
    @Override
    public List<TickerData> apply(Long limit) {
        List<TickerData> records = new ArrayList<>();
        File source = new File(resource.getURI());
        long id = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(source))) {
            String line;
            while ((line = br.readLine()) != null && limit > id) {
                String[] values = line.split(COMMA_DELIMITER);
                records.add(new TickerData(values[0], values[1]));
                id++;
            }
        }
        return records;
    }

}
