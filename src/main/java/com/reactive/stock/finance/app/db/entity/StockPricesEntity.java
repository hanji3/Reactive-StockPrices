package com.reactive.stock.finance.app.db.entity;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class StockPricesEntity {
    private UUID stockId;
    private Map<String, BigDecimal> prices;

    public StockPricesEntity(UUID stockId) {
        this.stockId = stockId;
        this.prices = new HashMap<>();
    }

    public UUID getStockId() {
        return stockId;
    }

    public void setStockId(UUID stockId) {
        this.stockId = stockId;
    }

    public Map<String, BigDecimal> getPrices() {
        return prices;
    }

    public void addPrice(String name, BigDecimal price) {
        this.prices.put(name, price);
    }

    public void setPrices(Map<String, BigDecimal> prices) {
        this.prices = prices;
    }
}
