package com.reactive.stock.finance.app.apis;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reactive.stock.finance.app.messages.NewsDetails;
import com.reactive.stock.finance.app.messages.NewsParameters;
import com.reactive.stock.finance.app.messages.TickerData;
import lombok.SneakyThrows;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.yaml.snakeyaml.error.MissingEnvironmentVariableException;

import java.util.List;
import java.util.Optional;

@Service
public class PolygonNewsIntegration implements NewsUpdatesHandler {


    String ENV_ERROR = "no polygon.io auth credentials supplied";
    String API_KEY = Optional.of(System.getenv("POLYGON_API_KEY"))
            .orElseThrow(() -> new MissingEnvironmentVariableException(ENV_ERROR));

    //    shared constants
    String ORDER = "asc";
    String RESULTS = "results";

    private final String NEWS_REFERENCE = "https://api.polygon.io/v2/reference/news/";;

    //    news constants
    private static final String LIMIT = "10";
    private static final String SORT = "published_utc";



    @Autowired
    RestTemplate restTemplate;

//    get news api
//    https://api.polygon.io/v2/reference/news?ticker=AAPL&published_utc=2021-12-21
//    // &order=asc&limit=10&sort=published_utc&apiKey=WaVXu2KPYzNk5efIugnG06025R8D7Hmr

    @SneakyThrows
    @Override
    public List<NewsDetails> apply(NewsParameters parameters) {

        String requestUri = UriComponentsBuilder.fromHttpUrl(NEWS_REFERENCE)
                .queryParam("ticker", parameters.getTicker())
                .queryParam("published_utc", parameters.getPublishing())
                .queryParam("order", ORDER)
                .queryParam("limit", LIMIT)
                .queryParam("sort", SORT)
                .queryParam("apiKey", API_KEY)
                .toUriString();

        String results =
                new JSONObject(restTemplate
                        .getForObject(requestUri, String.class)).getString(RESULTS);

        ObjectMapper newsMapper = new ObjectMapper();
        List<NewsDetails> news = newsMapper.readValue(results, new TypeReference<List<NewsDetails>>() {
        });
        return news;
    }

}
