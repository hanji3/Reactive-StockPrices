package com.reactive.stock.finance.app.stock;

public class SymbolNotFoundError extends Throwable {
    private static final String SYMBOL_NOT_FOUND = "Symbol not found at market:: ";

    public SymbolNotFoundError(String message) {
        super(SYMBOL_NOT_FOUND + message);
    }
}
