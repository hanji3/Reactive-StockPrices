package com.reactive.stock.finance.app.apis;

import com.reactive.stock.finance.app.messages.TickerData;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

public interface TickersHandler extends Function<Long,List<TickerData>> {
}
