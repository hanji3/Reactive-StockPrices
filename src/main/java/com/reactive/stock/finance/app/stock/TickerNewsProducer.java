package com.reactive.stock.finance.app.stock;

import com.reactive.stock.finance.app.apis.NewsUpdatesHandler;
import com.reactive.stock.finance.app.apis.TickersHandler;
import com.reactive.stock.finance.app.messages.NewsDetails;
import com.reactive.stock.finance.app.messages.NewsParameters;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class TickerNewsProducer extends SessionProducer {

    private static final int INTERVAL = 7;
    private static final int PAGES = 1;
    @Autowired
    private NewsUpdatesHandler newsUpdatesHandler;


    @Autowired
    public TickerNewsProducer(TickersHandler handler, NewsSubscribable[] subscribers) {
        super(handler);
        ConnectableFlux<Object> publish = Flux
                .create(e -> {

                    Date startDate = DateUtils.addDays(new Date(), -INTERVAL);;
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

                    try {
                        while (true) {

                            List<List<NewsDetails>> news = new ArrayList<>();
                            for (Map.Entry<String, String> tag : tags.entrySet()
                            ) {
                                NewsParameters parameters = new NewsParameters(
                                        tag.getValue(),
                                        formatter.format(startDate), Long.valueOf(PAGES));
                                news.add(newsUpdatesHandler.apply(parameters));

                            }

                            e.next(news);

                            Thread.sleep(300000);
                        }
                    } catch (InterruptedException error) {
                        System.out.println("Can not sleep: " + error.getMessage());
                    }
                })
                .publish();

        for (Subscribable subscribable : subscribers) {
            publish.subscribe(subscribable.getConsumer());
        }

        Thread t = new Thread(publish::connect);
        t.start();
    }
}
