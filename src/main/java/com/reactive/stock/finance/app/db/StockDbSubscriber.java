package com.reactive.stock.finance.app.db;

import com.reactive.stock.finance.app.messages.StockData;
import com.reactive.stock.finance.app.stock.FinanceSubscribable;
import com.reactive.stock.finance.app.stock.Subscribable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.function.Consumer;

@Component
public class StockDbSubscriber implements FinanceSubscribable {
    private final StockRepository stockRepository;

    @Autowired
    public StockDbSubscriber(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    @Override
    public Consumer<Object> getConsumer() {
        return data -> {
            try {
                stockRepository.save((StockData) data);
            } catch (SQLException e) {
                System.out.println("Can not save stock data: " + e.getMessage());
                e.printStackTrace();
            }
        };
    }
}
