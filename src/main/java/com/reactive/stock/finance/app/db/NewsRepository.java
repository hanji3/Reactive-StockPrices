package com.reactive.stock.finance.app.db;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reactive.stock.finance.app.messages.NewsDetails;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.json.JSONArray;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class NewsRepository {

    private final DataSource dataSource;

    public NewsRepository() {
        HikariConfig config = new HikariConfig();
        config.setDriverClassName("org.postgresql.Driver");

        config.setJdbcUrl(String.format("jdbc:postgresql:///%s", System.getenv("DB_NAME")));
        config.setUsername(System.getenv("DB_USER"));
        config.setPassword(System.getenv("DB_PASSWORD"));

        this.dataSource = new HikariDataSource(config);
    }

    public void save(NewsDetails data) throws SQLException {
        Connection connection = this.dataSource.getConnection();

        String saveNewsData = "insert into stocks" +
                " (id, title, description, author, timestamp, image, url, keywords) " +
                "values (?, ?, ?, ?, ?, ?, ?, ?);";

        UUID id = UUID.randomUUID();

        PreparedStatement pstmt = connection.prepareStatement(saveNewsData);
        pstmt.setObject(1, id, java.sql.Types.OTHER);
        pstmt.setString(2, data.getTitle());
        pstmt.setString(3, data.getDescription());
        pstmt.setString(4, data.getAuthorName());
        pstmt.setLong(5, data.getPublicationTime().getTime());
        pstmt.setString(6, data.getImage());
        pstmt.setString(7, data.getUrl());
        pstmt.setString(8, new JSONArray(data.getKeywords()).toString());
        pstmt.execute();

        connection.close();
    }

    public List<NewsDetails> getAll() throws SQLException, JsonProcessingException {
        Connection connection = this.dataSource.getConnection();
        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("select * from news order by timestamp");

        List<NewsDetails> news = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        while (resultSet.next()) {
            NewsDetails instance = new NewsDetails(

                    resultSet.getString("authorName"),
                    resultSet.getString("description"),
                    resultSet.getString("title"),
                    mapper.readValue(resultSet.getString("keywords"), new TypeReference<List<String>>(){}),
                    resultSet.getString("timestamp"),
                    resultSet.getString("image"),
                    resultSet.getString("url")

            );
            news.add(instance);
        }

        connection.close();
        return news;
    }

}
