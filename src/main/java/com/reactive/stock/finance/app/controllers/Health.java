package com.reactive.stock.finance.app.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class Health {
    @GetMapping("/health")
    public String health() {
        return "ok.";
    }
}
