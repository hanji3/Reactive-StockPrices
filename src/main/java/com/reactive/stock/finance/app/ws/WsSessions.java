package com.reactive.stock.finance.app.ws;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class WsSessions {
    private static WsSessions wsSessions;
    private List<WebSocketSession> webSocketSessions;
    private HashMap<WebSocketSession, String[]> sessionNames;

    private WsSessions() {
        this.webSocketSessions = new ArrayList<>();
        this.sessionNames = new HashMap<>();
    }

    public HashMap<WebSocketSession, String[]> getSessionNames() {
        return sessionNames;
    }

    public void setNamesForSession(WebSocketSession session, String[] names) {
        this.sessionNames.put(session, names);
    }

    public void addSession(WebSocketSession session) {
        this.webSocketSessions.add(session);
    }

    public void removeSession(WebSocketSession session) {
        this.webSocketSessions.remove(session);
    }

    public List<WebSocketSession> getWebSocketSessions() {
        return webSocketSessions;
    }

    public static WsSessions getInstance() {
        if (wsSessions == null) {
            wsSessions = new WsSessions();
        }
        return wsSessions;
    }
}
