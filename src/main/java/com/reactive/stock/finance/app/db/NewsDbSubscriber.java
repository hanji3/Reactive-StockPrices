package com.reactive.stock.finance.app.db;

import com.reactive.stock.finance.app.messages.NewsDetails;
import com.reactive.stock.finance.app.stock.NewsSubscribable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.function.Consumer;

@Component
public class NewsDbSubscriber implements NewsSubscribable {
    private final NewsRepository newsRepository;

    @Autowired
    public NewsDbSubscriber(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
    }

    @Override
    public Consumer<Object> getConsumer() {
        return data -> {
            try {
                newsRepository.save((NewsDetails) data);
            } catch (SQLException e) {
                System.out.println("Can not save stock data: " + e.getMessage());
                e.printStackTrace();
            }
        };
    }
}
