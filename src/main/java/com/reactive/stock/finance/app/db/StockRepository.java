package com.reactive.stock.finance.app.db;

import com.reactive.stock.finance.app.db.entity.StockEntity;
import com.reactive.stock.finance.app.db.entity.StockPricesEntity;
import com.reactive.stock.finance.app.messages.StockData;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.stereotype.Repository;
import com.zaxxer.hikari.HikariConfig;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Repository
public class StockRepository {

    private final DataSource dataSource;

    public StockRepository() {
        HikariConfig config = new HikariConfig();
        config.setDriverClassName("org.postgresql.Driver");

        config.setJdbcUrl(String.format("jdbc:postgresql:///%s", System.getenv("DB_NAME")));
        config.setUsername(System.getenv("DB_USER"));
        config.setPassword(System.getenv("DB_PASSWORD"));

        this.dataSource = new HikariDataSource(config);
    }

    private void savePrices(Connection connection, Map<String, BigDecimal> values, UUID stockId) throws SQLException {
        String savePrice = "insert into prices (id, stock_id, name, value) values (?, ?, ?, ?)";

        for (Map.Entry<String, BigDecimal> price: values.entrySet()) {
            PreparedStatement pstmt = connection.prepareStatement(savePrice);
            pstmt.setObject(1, UUID.randomUUID(), java.sql.Types.OTHER);
            pstmt.setObject(2, stockId, java.sql.Types.OTHER);
            pstmt.setString(3, price.getKey());
            pstmt.setBigDecimal(4, price.getValue());
            pstmt.execute();
        }
    }

    public void save(StockData data) throws SQLException {
        Connection connection = this.dataSource.getConnection();
        String saveStockData = "insert into stocks (id, name, timestamp) values (?, ?, ?);";

        UUID id = UUID.randomUUID();

        PreparedStatement pstmt = connection.prepareStatement(saveStockData);
        pstmt.setObject(1, id, java.sql.Types.OTHER);
        pstmt.setString(2, data.getName());
        pstmt.setLong(3, data.getTimestamp());
        pstmt.execute();

        this.savePrices(connection, data.getValues(), id);

        connection.close();
    }

    public StockPricesEntity getStockPrice(Connection connection, UUID stockId) throws SQLException {
        StockPricesEntity pricesEntity = new StockPricesEntity(stockId);

        PreparedStatement preparedStatement = connection.prepareStatement("select name, value from prices where stock_id=?");
        preparedStatement.setObject(1, stockId, java.sql.Types.OTHER);

        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            pricesEntity.addPrice(resultSet.getString("name"), resultSet.getBigDecimal("value"));
        }

        return pricesEntity;
    }

    public List<StockData> getAll() throws SQLException {
        Connection connection = this.dataSource.getConnection();
        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("select * from stocks order by timestamp");

        List<StockData> stocks = new ArrayList<>();
        while (resultSet.next()) {
            StockEntity stockEntity = new StockEntity(
                    UUID.fromString(resultSet.getString("id")),
                    resultSet.getString("name"),
                    resultSet.getLong("timestamp")
            );

            StockPricesEntity pricesEntity = this.getStockPrice(connection, stockEntity.getId());

            stocks.add(new StockData(stockEntity.getName(), stockEntity.getTimestamp(), pricesEntity.getPrices()));
        }

        connection.close();
        return stocks;
    }
}
