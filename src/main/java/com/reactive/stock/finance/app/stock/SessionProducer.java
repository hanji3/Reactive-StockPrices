package com.reactive.stock.finance.app.stock;

import com.reactive.stock.finance.app.apis.TickersHandler;
import com.reactive.stock.finance.app.messages.TickerData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SessionProducer {

    protected Map<String, String> tags;
    private final Long DEFAULT_LIMIT = Long.valueOf(100);

    @Autowired
    public SessionProducer(TickersHandler tickersHandler) {
        Set<TickerData> tickers = tickersHandler.apply(DEFAULT_LIMIT).stream().collect(Collectors.toSet());
        this.tags = tickers.stream().collect(Collectors.toMap(TickerData::getPlainName, TickerData::getTicker));
    }
}
