package com.reactive.stock.finance.app.stock;

import java.util.function.Consumer;

public interface Subscribable {
    Consumer<Object> getConsumer();
}
