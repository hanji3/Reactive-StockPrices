package com.reactive.stock.finance.app.messages;

public class NewsParameters {
    private final String ticker;
    private final String publishing;
    private final Long pageId;

    public NewsParameters(String ticker, String publishing, Long pageId) {
        this.ticker = ticker;
        this.publishing = publishing;
        this.pageId = pageId;
    }

    public String getTicker() {
        return ticker;
    }

    public Long getPageId() {
        return pageId;
    }

    public String getPublishing() {
        return publishing;
    }
}
