package com.reactive.stock.finance.app.apis;

import com.reactive.stock.finance.app.messages.NewsDetails;
import com.reactive.stock.finance.app.messages.NewsParameters;

import java.util.List;
import java.util.function.Function;

public interface NewsUpdatesHandler extends Function<NewsParameters, List<NewsDetails>> {
}
