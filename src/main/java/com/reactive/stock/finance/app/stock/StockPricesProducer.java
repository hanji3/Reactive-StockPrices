package com.reactive.stock.finance.app.stock;

import com.reactive.stock.finance.app.apis.FinancePricesHandler;
import com.reactive.stock.finance.app.apis.TickersHandler;
import com.reactive.stock.finance.app.messages.StockData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class StockPricesProducer extends SessionProducer {

    @Autowired
    private FinancePricesHandler financeApiIntegration;

    @Autowired
    public StockPricesProducer(TickersHandler handler, FinanceSubscribable[] subscribers) {
        super(handler);
        ConnectableFlux<Object> publish = Flux
                .create(e -> {

                    try {
                        while (true) {
                            Map<String, BigDecimal> prices = new HashMap<>();


                            for (Map.Entry<String, String> tag : tags.entrySet()
                            ) {
                                try {
                                    prices.put(tag.getKey(),
                                            financeApiIntegration.apply(tag.getValue())
                                                    .orElseThrow(() -> new SymbolNotFoundError(tag.getValue())));
                                } catch (SymbolNotFoundError error) {
                                    System.out.println(error);
                                }

                            }


                            Date curDate = new Date();

                            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");

                            StockData data = new StockData(formatter.format(curDate), new Date().getTime(), prices);
                            e.next(data);

                            Thread.sleep(2000);
                        }
                    } catch (InterruptedException error) {
                        System.out.println("Can not sleep: " + error.getMessage());
                    }
                })
                .publish();

        for (Subscribable subscribable : subscribers) {
            publish.subscribe(subscribable.getConsumer());
        }

        Thread t = new Thread(publish::connect);
        t.start();
    }
}
