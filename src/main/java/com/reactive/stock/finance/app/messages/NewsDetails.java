package com.reactive.stock.finance.app.messages;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NewsDetails {

    @JsonProperty("author")
    private  String authorName;
    @JsonProperty("description")
    private String description;
    @JsonProperty("title")
    private String title;
    @JsonProperty("tickers")
    private List<String> keywords;

    @JsonProperty("published_utc")
    private Timestamp publicationTime;

    @JsonProperty("image_url")
    private String  image;
    @JsonProperty("article_url")
    private String url;

    public NewsDetails() {
    }

    public NewsDetails(String authorName,
                       String description,
                       String title,
                       List<String> keywords,
                       String publicationTime,
                       String image,
                       String url) {
        this.authorName = authorName;
        this.description = description;
        this.title = title;
        this.keywords = keywords;
        this.publicationTime = Timestamp.valueOf(publicationTime);
        this.image = image;
        this.url = url;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public Timestamp getPublicationTime() {
        return publicationTime;
    }

    public String getImage() {
        return image;
    }

    public String getUrl() {
        return url;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public void setPublicationTime(Timestamp publicationTime) {
        this.publicationTime = publicationTime;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(
                "{ \"title\": \"" + this.title + "\"," +
                        " \"description\": " + this.description + "," +
                        " \"author\": " + this.authorName + "," +
                        " \"image\": " + this.image + "," +
                        " \"source\": " + this.url + "," +
                        " \"publication\": " + this.publicationTime + "," +
                        " \"keywords\": {");

        keywords.forEach(key ->
        {
            builder.append('"');
            builder.append(key);
            builder.append('"');
            builder.append(',');

        });
        builder.deleteCharAt(builder.length() - 1);
        builder.append("} }");

        return builder.toString();
    }
}
