package com.reactive.stock.finance.app.ws;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.reactive.stock.finance.app.db.NewsRepository;
import com.reactive.stock.finance.app.db.StockRepository;
import com.reactive.stock.finance.app.messages.NewsDetails;
import com.reactive.stock.finance.app.messages.StockData;
import com.reactive.stock.finance.app.stock.FinanceSubscribable;
import com.reactive.stock.finance.app.stock.NewsSubscribable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.function.Consumer;

@Component
public class WsHandler extends TextWebSocketHandler
        implements FinanceSubscribable, NewsSubscribable {
    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        WsSessions.getInstance().addSession(session);

        session.sendMessage(new TextMessage(buildStockMessage()));
        session.sendMessage(new TextMessage(buildNewsMessage()));
    }

    private String buildNewsMessage() throws SQLException, JsonProcessingException {
        List<NewsDetails> news = this.newsRepository.getAll();
        int len = news.size();

        StringBuilder builder = new StringBuilder("[");
        for (int i = len >= 20 ? len - 20 : 0; i < len; i++) {
            builder.append(news.get(i).toString());
            builder.append(",");
        }
        builder.deleteCharAt(builder.length() - 1);
        builder.append("]");
        return builder.toString();
    }

    private String buildStockMessage() throws SQLException {
        List<StockData> stocks = this.stockRepository.getAll();
        int len = stocks.size();

        StringBuilder builder = new StringBuilder("[");
        for (int i = len >= 20 ? len - 20 : 0; i < len; i++) {
            builder.append(stocks.get(i).toString());
            builder.append(",");
        }
        builder.deleteCharAt(builder.length() - 1);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        WsSessions.getInstance().removeSession(session);
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        System.out.println("new ws message: " + message.toString());
    }

    @Override
    public Consumer<Object> getConsumer() {
        return data -> WsSessions.getInstance().getWebSocketSessions().forEach(session -> {
            try {
                session.sendMessage(new TextMessage(data.toString()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
