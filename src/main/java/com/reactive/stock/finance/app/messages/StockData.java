package com.reactive.stock.finance.app.messages;

import java.math.BigDecimal;
import java.util.Map;

public class StockData {
    private final String name;
    private final long timestamp;
    private final Map<String, BigDecimal> values;

    public StockData(String name, long timestamp, Map<String, BigDecimal> values) {
        this.name = name;
        this.timestamp = timestamp;
        this.values = values;
    }

    public String getName() {
        return name;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Map<String, BigDecimal> getValues() {
        return values;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("{ \"name\": \"" + this.name + "\", \"timestamp\": " + this.timestamp + ", \"values\": {");

        for (Map.Entry<String, BigDecimal> pair : this.values.entrySet()) {
            builder.append('"');
            builder.append(pair.getKey());
            builder.append('"');
            builder.append(':');
            builder.append(pair.getValue());
            builder.append(',');
        }
        builder.deleteCharAt(builder.length() - 1);
        builder.append("} }");

        return builder.toString();
    }
}
