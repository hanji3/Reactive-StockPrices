package com.reactive.stock.finance.app.apis;

import org.springframework.stereotype.Service;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Optional;

@Service
public class YahooIntegration implements FinancePricesHandler {

    @Override
    public Optional<BigDecimal> apply(String stockName) {
        Stock stock = null;
        try {
            stock = YahooFinance.get(stockName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (stock != null) {
            return Optional.of(stock.getQuote().getPrice());
        }
        return Optional.empty();
    }

}
