package com.reactive.stock.finance.app.messages;

import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.util.HashMap;

public class StockDataTest {
    @Test
    void stockData_toString_test() {
        HashMap<String, Float> prices = new HashMap<>();
        prices.put("BitCoin", 2380.0f);
        prices.put("DogeCoin", 2685.0f);
        prices.put("Ethereum", 1544.0f);
        StockData stockData = new StockData("12:49:15", 1641120585666L, prices);

        Assert.isTrue(
                stockData.toString().equals("{ \"name\": \"12:49:15\", \"timestamp\": 1641120585666, \"values\": {\"BitCoin\":2380.0,\"Ethereum\":1544.0,\"DogeCoin\":2685.0} }"),
                "Result JSON string are not valid"
        );
    }
}
